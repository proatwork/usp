<?php
class Proatwork_Usp_Model_Catalog_Categories extends Mage_Core_Model_Abstract
{
    /**
     * @return array
     * @throws Mage_Core_Exception
     */
    public function toOptionArray(){
        /**
         * http://magento.stackexchange.com/questions/7940/get-subcategories-of-parent-category-when-flat-categories-enabled
         */
        $storeId = null;
        foreach (Mage::app()->getWebsites() as $website) {
            foreach ($website->getGroups() as $group) {
                $stores = $group->getStores();
                foreach ($stores as $store) {
                    $storeId =  $store->getId();
                }
            }
        }
        if(!$storeId) return;
        $recursionLevel = 2;
        $cats = array();
        $parent = Mage::app()->getStore($storeId)->getRootCategoryId();
        $tree = Mage::getResourceModel('catalog/category_tree');
        $_categories = $tree->loadNode($parent)
            ->loadChildren($recursionLevel)
            ->getChildren();
        $tree->addCollectionData(null, false, $parent, true, false);
        if (count($_categories) > 0) {
            foreach ($_categories as $_category) {
                if (!$_category->getIsActive()) continue;

                $categoryName =  $_category->getName();
                $categoryId = $_category->getId();

                $cats[] = array(
                    'value'    => $categoryId,
                    'label'  => $categoryName
                );

                $_category = Mage::getModel('catalog/category')->load($_category->getId());
                $_subcategories = $_category->getChildrenCategories();
                if (count($_subcategories) > 0) {
                    foreach ($_subcategories as $_subcategory) {
                        $subcategoryName    = $_subcategory->getName();
                        $subcategoryId      = $_subcategory->getId();
                        $cats[] = array(
                            'value'    =>  $subcategoryId,
                            'label'  =>  '-' . $subcategoryName
                        );
                    }
                }
            }
        }
        return $cats;
    }
}