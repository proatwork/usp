<?php
class Proatwork_Usp_Block_Usp extends Mage_Core_Block_Template implements Mage_Widget_Block_Interface
{
    public function getTemplate()
    {
        return $this->getData('template') ?: "proatwork/usp/usp.phtml";
    }

    public function getCategoryUrl($id = false)
    {
        $id = $id ? $id : $this->getCategoryId();
        $category = Mage::getModel('catalog/category')->load($id);
        return $category->getUrl();
    }

    public function getBg()
    {
        if($this->getBackgroundType() == 'img'){
            $base = Mage::getBaseUrl(Mage_Core_Model_Store::URL_TYPE_MEDIA);
            $default = $this->getSkinUrl('koket_components/img/v2/strip_bg.jpg');
            return " style=\"background-image: url('".$this->getBackground() ? $base . $this->getBackground() : $default."');\" ";
        } else {
            $hex = $this->getBackgroundHex();
            if(!strlen($hex)) return;
            if(strpos($hex, '#') == false) {
                $hex = "#" . $hex;
            }
            return " style=\"background: ".$hex." \" ";
        }
    }
    public function getTextCol()
    {
        $hex = $this->getTextColor();
        if(!strlen($hex)) {
            $hex = "fff";
        }
        if(strpos($hex, '#') == false) {
            $hex = "#" . $hex;
        }
        return " style=\"color: ".$hex." \" ";
    }

    public function getLinkColors()
    {
        $hex = $this->getTextColor();
        if(!strlen($hex)) {
            $hex = "fff";
        }
        if(strpos($hex, '#') == false) {
            $hex = "#" . $hex;
        }
        return " onmouseover=\"this.style.color='" . $hex . "'\" ";
    }

    public function getTheUrl()
    {
        return $this->getCustomUrlBool() ? $this->getCustomUrl() : $this->getCategoryUrl();
    }

    public function getTheIconUrl(){
        if(!$this->getIcon()) return Mage::helper('nwt_koket')->getMiddagLogo();
        return Mage::getBaseUrl('media') . $this->getIcon();
    }
}